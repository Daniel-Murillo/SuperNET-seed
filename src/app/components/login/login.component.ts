import 'rxjs/add/operator/finally';

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { environment } from '../../../environments/environment';
import { AuthenticationService } from '../../services/authentication/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  version: string = environment.version;
  error: string = null;
  loginForm: FormGroup;
  isLoading = false;

  constructor(private router: Router,
              private formBuilder: FormBuilder,
             private authenticationService: AuthenticationService) {
    this.createForm();
  }

  ngOnInit() {
    console.log(this.start());
  }

  start() {
    this.authenticationService.start().subscribe(
      success => console.log(success),
      error => console.log(error)
    );
  }

  login() {
    this.isLoading = true;
    this.authenticationService.login(this.loginForm.value)
      .finally(() => {
        this.loginForm.markAsPristine();
        this.isLoading = false;
      })
      .subscribe(credentials => {
        this.router.navigate(['home'], { replaceUrl: true });
      }, error => {
        this.error = error;
      });
  }

  private createForm() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
      remember: true
    });
  }

}
