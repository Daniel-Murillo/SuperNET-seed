import { Component, OnInit } from '@angular/core';

import { HomeService } from './services/home.services';

@Component({
  selector: 'super-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  date: any;
  constructor(private homeService: HomeService) {
    this.start();
  }

  ngOnInit() {}

  start() {
    this.homeService.getDate().subscribe(
      response => {
        console.log(response);
        this.date = response;
      },
      error => console.log(error)
    );
  }
}
