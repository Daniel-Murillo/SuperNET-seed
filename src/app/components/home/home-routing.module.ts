import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RouteService } from '../../services/route/route.service';
import { HomeComponent } from './home.component';

const routes: Routes = RouteService.withShell([
  { path: 'home', component: HomeComponent }
]);

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class HomeRoutingModule { }
