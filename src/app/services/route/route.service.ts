import { Routes } from '@angular/router';

import { ShellComponent } from '../../core/shell/shell.component';
// import { AuthenticationGuard } from '../../guard/authentication/authentication.guard';

/**
 * Provides helper methods to create routes.
 */
export class RouteService {

  /**
   * Creates routes using the shell component and authentication.
   * @param routes The routes to add.
   * @return {Routes} The new routes using shell as the base.
   */
  static withShell(routes: Routes): Routes {
    return [{
      path: '',
      component: ShellComponent,
      children: routes,
    //   canActivate: [AuthenticationGuard]
    }];
  }

}
