import { NgModule } from '@angular/core';

import { RouteService } from './route/route.service';

@NgModule({
    providers: [
        RouteService
    ]
})
export class ServicesModule { }
