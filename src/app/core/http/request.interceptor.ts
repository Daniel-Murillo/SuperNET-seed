import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/observable';

import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { environment } from '../../../environments/environment';

// import { AuthenticationService } from '../../services/authentication/authentication.service';

@Injectable()
export class RequestInterceptor implements HttpInterceptor {
    private configReq: object;

    constructor(/* private auth: AuthenticationService */) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (environment.tradicional) {
            req = req.clone({
                url: `${environment.urlBase + req.url}`,
                body: 'json=' + JSON.stringify(req.body)
            });
        } else {
            req = req.clone({
                url: `${environment.urlBase + req.url}`
            });
        }

        return next.handle(req);
    }
}
