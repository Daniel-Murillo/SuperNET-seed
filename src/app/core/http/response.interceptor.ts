import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/observable';
import 'rxjs/add/operator/do';

import { Router } from '@angular/router';

import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { AuthenticationService } from '../../services/authentication/authentication.service';


@Injectable()
export class ResponseInterceptor implements HttpInterceptor {
  constructor(private router: Router) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    return next.handle(req).do((event: HttpEvent<any>) => {
      // console.log(req);
      if (event instanceof HttpResponse) {
        // Cuando la respuesta es hecha correctamente
        // console.log(event.status);
        switch (event.status) {
          case 200:
            // console.log(event.body);
            if (event.body.error.clave === 'OK') {
              console.log(event.body.dto);
            }
            break;
        }
      } else if (event instanceof HttpErrorResponse) {
        console.log(event);
        switch (event.status) {
          case 404:
            console.log('rute not found');
            break;
          case 504:
            console.log('Puerto o url fuera de tiempo de espera.');
            break;
        }
      } else {
        console.log('ERR_CONNECTION_REFUSED');
      }
    });
  }
}
