import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { CookieService } from 'ngx-cookie-service';

import { InterceptorsModule } from './http/interceptor.module';

import { ShellComponent } from './shell/shell.component';
import { HeaderComponent } from './shell/header/header.component';

// import { AuthenticationService } from './authentication/authentication.service';
// import { AuthenticationGuard } from './authentication/authentication.guard';

// import { HttpService } from './http/http.service';
// import { HttpCacheService } from './http/http-cache.service';

export function createHttpService() { }

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    InterceptorsModule,
    RouterModule,
    FormsModule
  ],
  declarations: [
    HeaderComponent,
    ShellComponent
  ],
  providers: [
    CookieService
  ]
})
export class CoreModule {

  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    // Import guard
    if (parentModule) {
      throw new Error(`${parentModule} has already been loaded. Import Core module in the AppModule only.`);
    }
  }

}
